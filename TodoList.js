var vm = new Vue({
    el:'#app',
    data:{
        text:"",
        list:[
            // { id: 1 , name:'李清照' , status:false } ,
            // { id: 2 , name:'李清照' , status:false } 
        ] 
    },
    methods:{
        add(){
            let obj={id:this.list.length+1,name:this.text,status:false};
            this.list.push(obj);
            this.text="";
        },
        changeStatus(item){
            item.status=!item.status;
        },
        del(item,index){
            console.log(index)
            this.list.splice(index,1);
            this.updata(index)
        },
        updata(index){
            for(let i=0; i<this.list.length; i++){
                for(let index in this.list[i]){
                    this.list[i].id = i+1;
                }
            }
                
            }
    },
    computed:{
        unFinishCount(){
            let arr=this.list.filter((item)=>{
                return item.status==false;
            })
            return arr.length;
        }
    }
})