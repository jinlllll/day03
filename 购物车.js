let vm=new Vue({
    el:"#app",
    data:{
        list:[
            { name:'秋刀鱼',price:88,pic:"1.jfif",count:1 },
            { name:'大黄鱼',price:777,pic:"2.jfif",count:2 },
            { name:'皮皮虾',price:9,pic:"3.jfif",count:2 }
        ]},
        methods:{
            add(item){
                if(item.count<5)
                item.count++;
            },
            sub(item){
                if(item.count>1)
                item.count--;
            },
            clean(){
                this.list=[];
            }
        },
        computed:{
            totalCount(){
                var sum=0;
                this.list.forEach(item=>sum+=item.count);
                return sum;
            },
            totalPrice(){
                var sum=0;
                this.list.forEach(item=>{
                    sum=sum+item.count*item.price;
                });
                return sum;
            }
        }
        
    })